## Drone deployment

This repo is used to deploy [drone](https://drone.io/). This is a [Continuous
Integration](https://en.wikipedia.org/wiki/Continuous_integration) and
[Continuous Delivery](https://en.wikipedia.org/wiki/Continuous_delivery) software
used to deploy FSFE services, including:

- [Mailtrain](https://git.fsfe.org/fsfe-system-hackers/mailtrain)
- [Our discourse](https://git.fsfe.org/fsfe-system-hackers/community) instance
- The [reverse proxy](https://git.fsfe.org/fsfe-system-hackers/reverse-proxy)
- Our [piwiki](https://git.fsfe.org/fsfe-system-hackers/piwik) instance
- The [publiccode.eu](https://git.fsfe.org/pmpc/website) website
- The [savecodeshare.eu](https://git.fsfe.org/art13/website) website
- [Wekan](https://git.fsfe.org/fsfe-system-hackers/wekan)
- [ZoneMTA](https://git.fsfe.org/fsfe-system-hackers/zonemta)

## How to deploy Drone?

Run the following command from the computer where your GPG private key is:

```bash
ansible-playbook -i hosts drone.deploy.yml
```

The vault passphrase [vault passphrase](cred/vault_passphrase.gpg) is encrypted
with the following GPG keys:

```
gpg: encrypted with 4096-bit RSA key, ID 8B2FE9E6BE36E817, created 2015-07-26
      "Max Mehl <mail@mehl.mx>"
gpg: encrypted with 4096-bit RSA key, ID 71B6D4B7917020CE, created 2012-06-26
      "Albert Dengg (FSFE) <albert@fsfe.org>"
gpg: encrypted with 4096-bit RSA key, ID 94E4C04A3FE8AAC9, created 2014-01-04
      "Matthias Kirschner <mk@fsfe.org>"
gpg: encrypted with 2048-bit RSA key, ID CB3A5ACDBA0C288F, created 2016-01-27
      "Reinhard Müller <reinhard.mueller@bytewise.at>"
gpg: encrypted with 2048-bit RSA key, ID 2EE70F9CB77E590A, created 2015-09-15
      "Vincent Lequertier <vincent@fsfe.org>"
```

The file [open\_the\_vault.sh](cred/open_the_vault.sh) decrypts the passphrase
required to open the vault.
